package com.example.fame.kotlinproject.manager.http

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * Created by Fame on 5/18/2017 AD.
 */
public interface ApiService {
    @POST("list")
    fun loadPhotoList(): Call<ResponseBody>
}