package com.example.fame.kotlinproject.manager

import android.content.Context
import com.example.fame.kotlinproject.constant.Contextor
import com.example.fame.kotlinproject.manager.http.ApiService
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Fame on 5/18/2017 AD.
 */


class RetrofitHttpManager {


    companion object {
        internal var instance: RetrofitHttpManager? = null

        fun getInstance(): RetrofitHttpManager {
            if (instance == null)
                instance = RetrofitHttpManager()
            return instance as RetrofitHttpManager
        }
    }

    private var service: ApiService? = null
    private var mContext: Context ?= null

    private var gson = GsonBuilder().setLenient().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create()
    private fun RetrofitHttpManager(){
        mContext = Contextor.getInstance().context

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
        val client = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()


        val retrofit = Retrofit.Builder()
                .baseUrl("http://tmlth-eapp-dev.appmanproject.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()

        service = retrofit.create(ApiService::class.java)
    }

    fun getService(): ApiService {
        return service as ApiService
    }

}