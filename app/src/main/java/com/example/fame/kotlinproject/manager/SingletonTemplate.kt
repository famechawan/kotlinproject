package com.example.fame.kotlinproject.manager

import android.content.Context
import com.example.fame.kotlinproject.constant.Contextor

/**
 * Created by Fame on 5/19/2017 AD.
 */

class SingletonTemplate {

    companion object {
        internal var instance: SingletonTemplate? = null

        fun getInstance(): SingletonTemplate {
            if (instance == null)
                instance = SingletonTemplate()
            return instance as SingletonTemplate
        }
    }

    private var mContext: Context? = null

    private fun SingletonTemplate() {
        mContext = Contextor.instance?.context
    }
}