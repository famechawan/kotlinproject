package com.example.fame.kotlinproject.adapter

import android.support.v4.media.VolumeProviderCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.fame.kotlinproject.holder.PhotoListHolder
import android.widget.TextView
import com.example.fame.kotlinproject.holder.PhotoListHolder2
import com.example.fame.kotlinproject.view.PhotoListItem


/**
 * Created by Fame on 5/23/2017 AD.
 */


class PhotoListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val VIEW_TYPE_FIRST = 0
    private val VIEW_TYPE_SECOND = 1

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_FIRST -> {
                return PhotoListHolder(PhotoListItem(parent!!.context))
            }
            VIEW_TYPE_SECOND -> {
                var textView = TextView(parent?.context)
                return PhotoListHolder2(textView)
            }
            else -> {
                return PhotoListHolder(PhotoListItem(parent!!.context))
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
    }

    override fun getItemCount(): Int {
        return 10000
    }

    override fun getItemViewType(position: Int): Int {
        return position % 2
    }

}