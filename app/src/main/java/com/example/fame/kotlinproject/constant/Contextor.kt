package com.example.fame.kotlinproject.constant

import android.content.Context

/**
 * Created by Fame on 5/18/2017 AD.
 */
class Contextor private constructor() {
    var context: Context? = null
        private set

    fun init(context: Context) {
        this.context = context
    }

    companion object {
        internal var instance: Contextor? = null

        fun getInstance(): Contextor {
            if (instance == null) {
                instance = Contextor()
            }
            return instance as Contextor
        }

        fun clear() {
            instance = null
        }
    }
}
