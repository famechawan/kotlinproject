package com.example.fame.kotlinproject.holder

/**
 * Created by Fame on 5/23/2017 AD.
 */

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.example.fame.kotlinproject.R


class PhotoListHolder(view: View) : RecyclerView.ViewHolder(view) {
    var ivTitle: ImageView

    init {
        ivTitle = itemView.findViewById(R.id.ivImg) as ImageView
    }

}

