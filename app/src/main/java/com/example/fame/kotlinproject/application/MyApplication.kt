package com.example.fame.kotlinproject.application

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import com.example.fame.kotlinproject.R
import com.example.fame.kotlinproject.constant.Contextor

/**
 * Created by Fame on 5/18/2017 AD.
 */


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        //        initRealm();
        Contextor.getInstance().init(applicationContext)
    }


    override fun onLowMemory() {
        super.onLowMemory()
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
    }

    override fun onTerminate() {
        super.onTerminate()
        Contextor.clear()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }

}
